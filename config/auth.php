<?php

return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'customers',
    ],

    'guards' => [
        'api' => [
            'driver' => 'jwt',
            'provider' => 'customers',
        ],
    ],

    'providers' => [
        'customers' => [
            'driver' => 'eloquent',
            'model' => \App\Models\Customer::class
        ]
    ]
];