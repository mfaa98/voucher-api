<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'voucher_group',
        'voucher_code',
        'is_locked',
        'is_allocated',
        'is_redeemed', 
        'locked_at',
    ];
}
