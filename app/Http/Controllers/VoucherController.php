<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Voucher;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VoucherController extends Controller
{
    const ELIGIBLESTATUS = [
        'ELIGIBLE' => 'Customer is eligible',
        'NOT_ELIGIBLE' => 'Customer is not eligible for this promotion',
        'HAVE_VOUCHER' => 'Customer already have voucher',
    ];

    const VOUCHERSTATUS = [
        'VOUCHER_OUT' => 'No more available voucher',
        'VOUCHER_LOCKED' => 'Voucher is locked successfully',
    ];

    const SUBMITSTATUS = [
        'IMAGE_INVALID' => 'Image is invalid',
        'VOUCHER_INVALID' => 'Voucher is invalid or expired',
        'VOUCHER_ALLOCATED' => 'Voucher allocated successfully',
        'NO_VOUCHER' => 'Customer don\'t have voucher to submit',
    ];

    var $config = [
        'duration' => 30,
        'minTransactionCount' => 3,
        'minTransactionSum' => 100,
        'voucherGroup' => 1,
        'maxVoucher' => 1,
        'maxLockMinutes' => 10,
    ];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:api', []);
    }

    public function checkCustomerEligible(Request $request)
    {
        $customerId = auth()->user()->id;

        $options = $this->config;
        $options['customerId'] = $customerId;

        // check eligible
        $eligible = $this->_isCustomerEligible($options);

        if($eligible != $this::ELIGIBLESTATUS['ELIGIBLE']) return $this->core->setResponse('error', $eligible);
        
        if(isset($request->all()['noLock'])) return $this->core->setResponse('success', $eligible);

        // get new voucher, or return current voucher
        $voucherCode = $this->_lockVoucher($options);

        if(is_null($voucherCode)) return $this->core->setResponse('error', $this::VOUCHERSTATUS['VOUCHER_OUT']);

        // return vouchercode
        return $this->core->setResponse('success', $this::VOUCHERSTATUS['VOUCHER_LOCKED'], $voucherCode);
    }

    public function validatePhotoSubmission(Request $request)
    {
        $input = $request->all();
        $voucherCode = $input['voucherCode'] ?? null; // in case user can have more than one voucher

        $customerId = auth()->user()->id;
        $lockedAt = Carbon::now()->subMinutes($this->config['maxLockMinutes']);

        // check if voucher exist
        $query = Voucher::where('customer_id', $customerId)
                ->whereNotNull('locked_at');

        if(!is_null($voucherCode)) $query->where('voucher_code', $voucherCode);
        $voucher = $query->first();

        if(is_null($voucher)) return $this->core->setResponse('error', $this::SUBMITSTATUS['NO_VOUCHER']);
        $voucherCode = $voucher->voucher_code;
        
        // check if voucher under max lock time
        $lockTime = Carbon::now()->diffInMinutes(Carbon::parse($voucher->locked_at));
        if($lockTime > $this->config['maxLockMinutes']){
            $this->_releaseVoucher($voucherCode, $customerId);
            return $this->core->setResponse('error', $this::SUBMITSTATUS['VOUCHER_INVALID']);
        }

        // call image validation api
        $imageValid = $this->_callValidationApi($request);

        if(!$imageValid){
            $this->_releaseVoucher($voucherCode, $customerId);
            return $this->core->setResponse('error', $this::SUBMITSTATUS['IMAGE_INVALID']);
        }
                
        // validation successful, voucher is yours
        $this->_allocateVoucher($voucherCode, $customerId);
        return $this->core->setResponse('success', $this::SUBMITSTATUS['VOUCHER_ALLOCATED']);
    }

    private function _isCustomerEligible($data)
    {        
        // Eligible means:
        // - Complete 3 purchase transactions within the last 30 days. (AND)
        // - Total transactions equal or more than $100. (AND)
        // - Each customer is allowed to redeem 1 cash voucher only. (AND)

        $data['pastDay'] = Carbon::today()->subDays($data['duration']);
        // die(json_encode($data));

        //check for eligible customer by transaction
        $count = Customer::select("customers.id")
                ->leftJoin('purchase_transactions', function($join) use ($data){
                    $join->on('purchase_transactions.customer_id', 'customers.id')
                        ->where('customers.id', $data['customerId'])
                        ->where('purchase_transactions.transaction_at', '>=', $data['pastDay']);
                })
                ->havingRaw('sum(purchase_transactions.total_spent) >= ?', [$data['minTransactionSum']])
                ->havingRaw('count(purchase_transactions.id) >= ?', [$data['minTransactionCount']])
                ->groupBy('customers.id')
                ->count();

        if($count != 1){
            $this->core->log('debug', "INVALID RESULT: Customer returns $count data.");
            return $this::ELIGIBLESTATUS['NOT_ELIGIBLE'];
        }

        // check for eligible customer by voucher
        // not eligible if customer already have maximum number of voucher
        $voucherCount = Voucher::where('customer_id', $data['customerId'])
                -> where('vouchers.voucher_group', $data['voucherGroup'])
                -> where('vouchers.is_allocated', true) -> count();
        
        if($voucherCount >= $data['maxVoucher']){
            $this->core->log('debug', "CUSTOMER HAS VOUCHER: Customer already have $voucherCount voucher.");
            return $this::ELIGIBLESTATUS['HAVE_VOUCHER'];
        }
        
        return $this::ELIGIBLESTATUS['ELIGIBLE'];
    }

    private function _lockVoucher($data)
    {
        // if found already locked, return this voucher, no updates. 
        $voucher = Voucher::where('customer_id', $data['customerId'])
                    ->where('voucher_group', $data['voucherGroup'])
                    ->whereNotNull('locked_at')
                    ->first();

        if(!is_null($voucher)){
            // die(json_encode($voucher->toArray()));
            $lockedAt = Carbon::now()->diffInMinutes(Carbon::parse($voucher->locked_at));

            // voucher still locked
            if($lockedAt < $data['maxLockMinutes']) return $voucher->voucher_code;

            // already pass the time. should get another
            $this->_releaseVoucher($voucher->voucher_code, $data['customerId']);
        }


        // look for voucher that is not locked and not allocated
        $lockedAt = Carbon::now()->subMinutes($data['maxLockMinutes']);
        $affected = DB::statement("
            UPDATE vouchers SET 
                locked_at = ?,
                customer_id = ?
            WHERE voucher_group = ?
                AND is_allocated = ?
                AND ( locked_at IS NULL OR locked_at < ? )
            ORDER BY id
            LIMIT 1;
        ", [
            Carbon::now(), 
            $data['customerId'], 
            $data['voucherGroup'],
            false,
            $lockedAt
        ]);
        
        if(!$affected) return null;

        $voucher = Voucher::where(['customer_id'=>$data['customerId']])->first();
        // die($voucher->voucher_code);
        if(!$voucher) return null;

        return $voucher->voucher_code;
    }

    private function _allocateVoucher($voucherCode, $customerId)
    {
        Voucher::where('voucher_code', $voucherCode)
                ->where('customer_id', $customerId)
                ->update(['is_allocated' => true]);
        
        return true;
    }

    private function _releaseVoucher($voucherCode, $customerId = null)
    {
        $query = Voucher::where('voucher_code', $voucherCode);

        if(!is_null($customerId)) $query->where('customer_id', $customerId);

        $query->update([
            'locked_at' => null,
            'customer_id' => null
        ]);

        return true;
    }

    private function _redeemVoucher($voucherCode, $customerId)
    {
        Voucher::where('voucher_code', $voucherCode)
                ->where('customer_id', $customerId)
                ->update(['is_redeemed' => true]);
        
        return true;

    }

    private function _callValidationApi(Request $request)
    {
        //TODO: call actual image processing api
        $input = $request->all();
        return $input['imageValidationSuccessDummy'] ?? true;
    }
}
