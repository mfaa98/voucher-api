<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Customer;

class AuthController extends Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:api', ['except' => ['register', 'login', 'refresh']]);
    }

    /**
     * register customer
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function register(Request $request) {

        /* validation requirement */
        $validator = $this->validation('registration', $request);

        if ($validator->fails()) {

            return $this->core->setResponse('error', $validator->messages()->first(), NULL, false , 400  );
        }

        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        $customer = Customer::create($input);

        return $this->core->setResponse('success', 'Account created successfully!', $customer );
    }

    /**
     * validation requirement
     *
     * @param  string $type
     * @param  request $request
     * @return object
     */
    private function validation($type = null, $request) {

        switch ($type) {

            case 'registration':

                $validator = [
                    'first_name' => 'required|max:255|min:2',
                    'last_name' => 'required|max:255|min:2',
                    'email' => 'required|email|unique:customers',
                    'password' => 'required|min:6|max:100',
                ];
                
                break;

            case 'login':

                $validator = [
                    'email' => 'required|string',
                    'password' => 'required|string',
                ];

                break;

            default:
                
                $validator = [];
        }

        return Validator::make($request->all(), $validator);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login()
    {
        /* validation requirement */
        $validator = $this->validation('login', request());

        if ($validator->fails()) {

            return $this->core->setResponse('error', $validator->messages()->first(), NULL, false , 400  );
        }

        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {

            return $this->core->setResponse('error', 'Please check your email or password !', NULL, false , 400  );
        }

        return $this->respondWithToken($token, 'login');
    }


     /**
     * Get the authenticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile() {

        return $this->core->setResponse('success', 'User Profile',  auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return $this->core->setResponse('success', 'Successfully logged out' );
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh(), 'refresh token');
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $action = null)
    {
        $data = [
            'access_token' => $token,
            'token_type' => 'bearer',
            'email' => auth()->user()->email, 
            'expires_in' => auth()->factory()->getTTL() * config('auth.jwt.expires_in', 60),
        ];

        return $this->core->setResponse('success', "Successfully $action", $data );
    }

}