<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->nullable();
            $table->bigInteger('voucher_group')->nullable();
            $table->string('voucher_code');
            $table->boolean('is_allocated')->default(0);
            $table->boolean('is_redeemed')->default(0);
            $table->timestamp('locked_at')->nullable();
            $table->timestamps();

            $table->unique(['voucher_group','voucher_code']);
            $table->index('customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
