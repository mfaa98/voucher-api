<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Libraries\Core;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('CustomersTableSeeder');
        DB::statement("TRUNCATE table customers;");
        DB::table('customers')->insert([
            [
                'id' => 1,
                'first_name' => 'Ayam',
                'last_name' => 'Ayam',
                'email' => 'ayam@test.com',
                'password' => Hash::make('password'),
            ],
            [
                'id' => 2,
                'first_name' => 'Bebek',
                'last_name' => 'Bebek',
                'email' => 'bebek@test.com',
                'password' => Hash::make('password'),
            ],
            [
                'id' => 3,
                'first_name' => 'Capung',
                'last_name' => 'Capung',
                'email' => 'capung@test.com',
                'password' => Hash::make('password'),
            ],
            [
                'id' => 4,
                'first_name' => 'Domba',
                'last_name' => 'Domba',
                'email' => 'domba@test.com',
                'password' => Hash::make('password'),
            ],
            [
                'id' => 5,
                'first_name' => 'Elang',
                'last_name' => 'Elang',
                'email' => 'elang@test.com',
                'password' => Hash::make('password'),
            ],
            [
                'id' => 6,
                'first_name' => 'Flamingo',
                'last_name' => 'Flamingo',
                'email' => 'flamingo@test.com',
                'password' => Hash::make('password'),
            ],
        ]);

        DB::statement("TRUNCATE table purchase_transactions;");
        DB::table('purchase_transactions')->insert([
            //eligible
            [
                'customer_id' => 1,
                'total_spent' => 10,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(40),
            ],
            [
                'customer_id' => 1,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(20),
            ],
            [
                'customer_id' => 1,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(10),
            ],
            [
                'customer_id' => 1,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(5),
            ],


            //not eligible spent
            [
                'customer_id' => 2,
                'total_spent' => 50,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(40),
            ],
            [
                'customer_id' => 2,
                'total_spent' => 20,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(20),
            ],
            [
                'customer_id' => 2,
                'total_spent' => 20,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(10),
            ],
            [
                'customer_id' => 2,
                'total_spent' => 20,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(5),
            ],


            //not eligible count
            [
                'customer_id' => 3,
                'total_spent' => 60,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(40),
            ],
            [
                'customer_id' => 3,
                'total_spent' => 60,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(20),
            ],
            [
                'customer_id' => 3,
                'total_spent' => 60,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(10),
            ],


            //not eligible both
            [
                'customer_id' => 4,
                'total_spent' => 100,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(40),
            ],
            [
                'customer_id' => 4,
                'total_spent' => 10,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(20),
            ],
            [
                'customer_id' => 4,
                'total_spent' => 10,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(10),
            ],

            //eligible already 7 mminutes
            [
                'customer_id' => 5,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(5),
            ],
            [
                'customer_id' => 5,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(4),
            ],
            [
                'customer_id' => 5,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(3),
            ],

            //not eligible, already have allocated voucher
            [
                'customer_id' => 6,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(6),
            ],
            [
                'customer_id' => 6,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(6),
            ],
            [
                'customer_id' => 6,
                'total_spent' => 40,
                'total_saving' => 10,
                'transaction_at' => Carbon::now()->subDays(6),
            ],

        ]);

        $core = new Core();
        DB::statement("TRUNCATE table vouchers;");
        DB::table('vouchers')->insert([
            [
                'voucher_group' => 1,
                'voucher_code' => $core->generateRandomString(6),
            ],
            [
                'voucher_group' => 1,
                'voucher_code' => $core->generateRandomString(6),
            ],
            [
                'voucher_group' => 1,
                'voucher_code' => $core->generateRandomString(6),
            ],
            [
                'voucher_group' => 1,
                'voucher_code' => $core->generateRandomString(6),
            ],
        ]);

        //customer 5 eligible already have locked for 7 minutes
        DB::table('vouchers')->insert([
            [
                'customer_id' => 5,
                'voucher_group' => 1,
                'voucher_code' => $core->generateRandomString(6),
                'locked_at' => Carbon::now()->subMinutes(7),
            ],
        ]);

        //customer 6 will not eligible already have allocated voucher
        DB::table('vouchers')->insert([
            [
                'customer_id' => 6,
                'voucher_group' => 1,
                'voucher_code' => $core->generateRandomString(6),
                'is_allocated' => true,
            ],
        ]);
    }
}
