# VOUCHER_API
Simple voucher api in lumen. Main function are:
- lock voucher while in progress
- commit voucher to customer

## Disclaimer
This api is a product of a particular test that is given to me. So please be aware that while you are able to clone this project, I will not maintain this.

## Getting started and install the API
```
git clone https://gitlab.com/mfaa98/voucher-api.git
cd voucher-api
composer install
```
Setup env and update database connection
```
cp .env.development .env
vi .env
```
```
APP_NAME="Lumen JWT"
...
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=yourdnname
DB_USERNAME=yourdbusername
DB_PASSWORD=yourdbpassord
...
```
Create App Key and JWT Secret
```
php artisan key:generate
php artisan jwt:secret
```
Migrate and Run
```
php artisan migrate
php -S localhost:8000 -t public
```

## API list:
- [POST] /api/check-customer-eligible

This api is used to check if logged in customer eligible for current promotion and then lock a voucher if eligible. The rules currently are hardcoded, but already put in a configuration variable.
```
    var $config = [
        'duration' => 30,
        'minTransactionCount' => 3,
        'minTransactionSum' => 100,
        'voucherGroup' => 1,
        'maxVoucher' => 1,
        'maxLockMinutes' => 10,
    ];
```
| Config Name | Description |
| --- | --- |
| duration | Transaction Duration in days. API will get last {duration} days from current time when called. |
| minTransactionCount | Minimum transaction count in last {duration} days that the customer are made. |
| minTransactionSum | The minimum value of transaction spent in last {duration} days that the customer were made. |
| voucherGroup | Which {voucherGroup} of promotion that the API will get |
| maxVoucher | Maximum voucher that a customer can have |
| maxLockMinutes | Maximum waiting time for a voucher that is locked by customer to be allocated before the voucher get release back to the voucher pool. Customer must do a validation-photo-submission to allocate the voucher. |

| Parameter | Description |
| --- | --- |
| none | none |

- [POST] /api/validate-photo-submission

This api is used to do customer data validation so the voucher status can be locked from 'locked' to 'allocated'. Allocated voucher will be redeemable by the Customer.

| Parameter | Description |
| --- | --- |
| none | none |

- [POST] /api/register

This api is used to register an user to Customer table

| Parameter | Description |
| --- | --- |
| email | [Mandatory] Customer valid email |
| password | [Mandatory] Customer valid password |
| first_name | [Mandatory] Customer first name |
| last_name | [Mandatory] Customer last name |

- [POST] /api/login

This api is used to login an user and get access token

| Parameter | Description |
| --- | --- |
| email | [Mandatory] Customer valid email |
| password | [Mandatory] Customer valid password |

- [GET] /api/logout

This api is used to logout the user

| Parameter | Description |
| --- | --- |
| none | none |

## Additional API List:
- [GET] /

show welcome

- [GET] /version

show lumen version

- [GET] /api/ping

show "pong"

- [GET] /api/version

show lumen version

## Unit Testing
```
.\vendor\bin\phpunit .\tests\[NAME].php
```
We have 5 Unit testing which are:
- RegistrationTest: To test customer registration, login, and logout features
- AuthTest: Testing Authentication for user to call certain api function
- CustomerEligibleTest: To test if customer is eligible to get promotion voucher
- VoucherLockTest: To test various Lock scenario when customer are eligible to lock the voucher
- VoucherSubmitTest: To test scenario after customer sends a validation form to make the voucher redeemable

## Feature Roadmap
- Redeem Voucher Feature
- Usage of voucher group so we can create more than one promotion altogether
- Add more settings to voucher
- Add automatic voucher creation feature
