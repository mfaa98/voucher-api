<?php

$router->post('/login', 'AuthController@login');
$router->post('/register', 'AuthController@register');

/* restrict route */
$router->group(['middleware' => 'auth'], function () use ($router) {
    
    /* get user profile */
    $router->get('/profile', [ 'as' => 'profile', 'uses' => 'AuthController@profile']);

    /* logout user */
    $router->get('/logout', [ 'as' => 'logout', 'uses' => 'AuthController@logout']);

    /* refresh token */
    $router->get('/refresh', [ 'as' => 'refresh', 'uses' => 'AuthController@refresh']);

   
});