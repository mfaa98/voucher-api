<?php

/* restrict route */
$router->group(['middleware' => 'auth'], function () use ($router) {
    
    /* check user if eligible, get and lock the voucher */
    $router->post('/check-customer-eligible', 'VoucherController@checkCustomerEligible');

    /* validate submission for photo, and process the voucher */
    $router->post('/validate-photo-submission', 'VoucherController@validatePhotoSubmission');

   
});