<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Carbon\Carbon;

use App\Http\Controllers\VoucherController;

class VoucherSubmitTest extends TestCase
{
    use DatabaseMigrations;

    public $token;

    public function setUp(): void {
        parent::setUp();
        Artisan::call('migrate:fresh',['--seed' => true]);
    }

    public function test_submit_without_voucher_user(){
        $this->setToken('ayam@test.com');

        $request = $this->post('/api/validate-photo-submission', [
            'imageValidationSuccessDummy' => true
        ]);

        $request->response->assertJson([
            'error' => VoucherController::SUBMITSTATUS['NO_VOUCHER']
        ])->assertStatus(400);

    }

    public function test_submit_success_user(){
        $this->setToken('elang@test.com');

        $request = $this->post('/api/validate-photo-submission', [
            'imageValidationSuccessDummy' => true
        ]);

        $request->response->assertJson([
            'success' => VoucherController::SUBMITSTATUS['VOUCHER_ALLOCATED']
        ])->assertStatus(200);

    }

    public function test_submit_image_invalid_user(){
        $this->setToken('elang@test.com');

        $request = $this->post('/api/validate-photo-submission', [
            'imageValidationSuccessDummy' => false
        ]);

        $request->response->assertJson([
            'error' => VoucherController::SUBMITSTATUS['IMAGE_INVALID']
        ])->assertStatus(400);

    }

    public function test_submit_voucher_expired_user(){
        $this->setToken('elang@test.com');

        DB::table('vouchers')->update([
            'locked_at' => Carbon::now()->subMinutes(15)
        ]);

        $request = $this->post('/api/validate-photo-submission', [
            'imageValidationSuccessDummy' => true
        ]);

        $request->response->assertJson([
            'error' => VoucherController::SUBMITSTATUS['VOUCHER_INVALID']
        ])->assertStatus(400);

    }


    private function setToken($email) {

        /* valid login */
        $this->post('/api/login', [
            'email' => $email,
            'password' => 'password',
        ]);

    }

}
