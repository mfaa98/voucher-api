<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * user token
     */
    public $token = null;

    /**
     * test validate wrong user or password login 
     *
     * @return void
     */
    public function test_validate_wrong_user_or_password()
    {

        /* wrong user */
        $request = $this->post('/api/login', [
            'email' => 'testmails@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'Please check your email or password !'
        ])->assertStatus(400);

        /* wrong password */
        $request = $this->post('/api/login', [
            'email' => 'testmail@testmail.com',
            'password' => 'secreto',
        ]);

        $request->response->assertJson([
            'error' => 'Please check your email or password !'
        ])->assertStatus(400);

    }

    /**
     * test unauthorized route auth middleware
     *
     * @return void
     */
    public function test_unauthorized_access_auth_middleware()
    {
        $request = $this->get('/api/profile');

        $request->response->assertJson([
            'message' => 'Unauthorized'
        ])->assertStatus(401);
    }
    
    /**
     * test validate valid login
     *
     * @return void
     */
    public function test_valid_login()
    {

        /* valid register */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'success' => 'Account created successfully!'
        ])->assertStatus(200);

        /* valid login */
        $request = $this->post('/api/login', [
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $data = $request->response->decodeResponseJson()?? [];

        if ($data['success'] ?? false) {

            $this->token = $data['data']['token'] ?? '';
        }

        $request->response->assertJson([
            'success' => 'Successfully login'
        ])->assertStatus(200);

    }
    
    /**
     * test authorized route auth middleware
     *
     * @return void
     */
    public function test_authorized_access_auth_middleware()
    {
        $this->setToken();

        $request = $this->get('/api/profile');

        $request->response->assertStatus(200);
    }
    
    /**
     * set token for auth
     *
     * @return void
     */
    private function setToken() {

        /* valid register */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        /* valid login */
        $this->post('/api/login', [
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

    }
}
