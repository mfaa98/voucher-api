<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Http\Controllers\VoucherController;

class VoucherLockTest extends TestCase
{
    use DatabaseMigrations;

    public $token;

    public function setUp(): void {
        parent::setUp();
        Artisan::call('migrate:fresh',['--seed' => true]);
    }

    public function test_eligible_voucher_user(){
        $this->setToken('ayam@test.com');

        $request = $this->post('/api/check-customer-eligible', [
        ]);

        $request->response->assertJson([
            'success' => VoucherController::VOUCHERSTATUS['VOUCHER_LOCKED']
        ])->assertStatus(200);

    }

    public function test_eligible_have_locked_voucher_user(){
        $this->setToken('elang@test.com');

        $request = $this->post('/api/check-customer-eligible', [
        ]);

        $request->response->assertJson([
            'success' => VoucherController::VOUCHERSTATUS['VOUCHER_LOCKED']
        ])->assertStatus(200);

    }

    public function test_eligible_no_more_voucher_user(){
        $this->setToken('elang@test.com');

        DB::table('vouchers')->update([
            'customer_id' => null,
            'is_allocated' => true
        ]);

        $request = $this->post('/api/check-customer-eligible', [
        ]);

        $request->response->assertJson([
            'error' => VoucherController::VOUCHERSTATUS['VOUCHER_OUT']
        ])->assertStatus(200);

    }

    private function setToken($email) {

        /* valid login */
        $this->post('/api/login', [
            'email' => $email,
            'password' => 'password',
        ]);

    }

}
