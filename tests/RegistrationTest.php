<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Libraries\Core;

class RegistrationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * test validate first_name properties
     *
     * @return void
     */
    public function test_validate_first_name_properties()
    {

        /* require */
        $request = $this->post('/api/register', [
            // 'first_name' => 'Nama',
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The first name field is required.'
        ])->assertStatus(400);

        /* minimum character >= 2 */
        $request = $this->post('/api/register', [
            'first_name' => 'J',
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The first name must be at least 2 characters.'
        ])->assertStatus(400);

        /* maximun character <= 255 */
        $request = $this->post('/api/register', [
            'first_name' => Core::generateRandomString(256),
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The first name may not be greater than 255 characters.'
        ])->assertStatus(400);

    }

    /**
     * test validate last_name properties
     *
     * @return void
     */
    public function test_validate_last_name_properties()
    {

        /* require */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            // 'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The last name field is required.'
        ])->assertStatus(400);

        /* minimum character >= 2 */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'D',
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The last name must be at least 2 characters.'
        ])->assertStatus(400);

        /* maximun character <= 255 */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => Core::generateRandomString(256),
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The last name may not be greater than 255 characters.'
        ])->assertStatus(400);

    }

    /**
     * test validate email properties
     *
     * @return void
     */
    public function test_validate_email_properties()
    {

        /* require */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'Coba',
            // 'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The email field is required.'
        ])->assertStatus(400);

        /* is email */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'Coba',
            'email' => 'testmail.email',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The email must be a valid email address.'
        ])->assertStatus(400);

    }

    /**
     * test validate last_name properties
     *
     * @return void
     */
    public function test_validate_password_properties()
    {

        /* require */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            // 'password' => 'secret',
        ]);

        $request->response->assertJson([
            'error' => 'The password field is required.'
        ])->assertStatus(400);

        /* minimum character >= 6 */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => 'secre',
        ]);

        $request->response->assertJson([
            'error' => 'The password must be at least 6 characters.'
        ])->assertStatus(400);

        /* maximun character <= 100 */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => Core::generateRandomString(101),
        ]);

        $request->response->assertJson([
            'error' => 'The password may not be greater than 100 characters.'
        ])->assertStatus(400);

    }

    /**
     * test validate email properties
     *
     * @return void
     */
    public function test_valid_registration()
    {

        /* require */
        $request = $this->post('/api/register', [
            'first_name' => 'Nama',
            'last_name' => 'Coba',
            'email' => 'testmail@testmail.com',
            'password' => 'secret',
        ]);

        $request->response->assertJson([
            'success' => 'Account created successfully!'
        ])->assertStatus(200);

    }


}
