<?php

use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Http\Controllers\VoucherController;

class CustomerEligibleTest extends TestCase
{
    use DatabaseMigrations;

    public $token;

    public function setUp(): void {
        parent::setUp();
        Artisan::call('migrate:fresh',['--seed' => true]);
    }

    public function test_eligible_user(){
        $this->setToken('ayam@test.com');

        $request = $this->post('/api/check-customer-eligible', [
            'noLock' => true,
        ]);

        $request->response->assertJson([
            'success' => VoucherController::ELIGIBLESTATUS['ELIGIBLE']
        ])->assertStatus(200);

    }

    public function test_not_eligible_spent_user(){
        $this->setToken('bebek@test.com');

        $request = $this->post('/api/check-customer-eligible', [
            'noLock' => true,
        ]);

        $request->response->assertJson([
            'error' => VoucherController::ELIGIBLESTATUS['NOT_ELIGIBLE']
        ])->assertStatus(400);

    }

    public function test_not_eligible_count_user(){
        $this->setToken('capung@test.com');

        $request = $this->post('/api/check-customer-eligible', [
            'noLock' => true,
        ]);

        $request->response->assertJson([
            'error' => VoucherController::ELIGIBLESTATUS['NOT_ELIGIBLE']
        ])->assertStatus(400);

    }

    public function test_not_eligible_both_user(){
        $this->setToken('domba@test.com');

        $request = $this->post('/api/check-customer-eligible', [
            'noLock' => true,
        ]);

        $request->response->assertJson([
            'error' => VoucherController::ELIGIBLESTATUS['NOT_ELIGIBLE']
        ])->assertStatus(400);

    }

    public function test_not_eligible_already_have_voucher_user(){
        $this->setToken('flamingo@test.com');

        $request = $this->post('/api/check-customer-eligible', [
            'noLock' => true,
        ]);

        $request->response->assertJson([
            'error' => VoucherController::ELIGIBLESTATUS['HAVE_VOUCHER']
        ])->assertStatus(400);

    }
    private function setToken($email) {

        /* valid login */
        $this->post('/api/login', [
            'email' => $email,
            'password' => 'password',
        ]);

    }    
}
